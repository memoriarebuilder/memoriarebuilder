#ifndef MAINUNT_H
#define MAINUNT_H

#include <QMainWindow>
#include <QFileDialog>

#pragma pack(push, 1)
typedef struct tagBlockHeader {
    quint16 MBR;
    quint16 Rev;
} TBlockHeader;
typedef struct tagChildLinkHeader {
    quint32 MBR;        // 51410101
    quint32 BlockCount; // 10, 490
    quint8 rev;         // 09
    quint32 Checksum;   // EA831000
} TChildLinkHeader;
#pragma pack(pop)

namespace Ui {
    class TMainFrm;
}

class TMainFrm : public QMainWindow {
    Q_OBJECT
public:
    explicit TMainFrm(QWidget *parent = 0);
    ~TMainFrm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TMainFrm *ui;
    QFileDialog OpenSaveFileDialog, OpenSaveDBDialog, OpenSaveMpkDialog;
private:
private:
    void BuildNodesViewFromUserData( QIODevice* userdbbuf );
private slots:
    void onImportUserClicked();
    void onImportMsgExClicked();
};

QString localLanguage();

#endif // MAINUNT_H
