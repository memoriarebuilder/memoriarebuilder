#include <QtGui/QApplication>
#include <QtCore/QSettings>
#include <QtCore/QFile>
#include <QtSql/QtSQL>
#include "DbCentre.h"

TPathRecItem PathSetting; // imp
TStateRecItem StateSetting; // imp

void LoadAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfMemoria.ini",
        QSettings::IniFormat);
    PathSetting.LastBuddyFolder = settings.value("Path/LastBuddyFolder", "").toString();
    PathSetting.LastStockXipFolder = settings.value("Path/LastStockXipFolder", "").toString();
    PathSetting.LastCustomXipFolder = settings.value("Path/LastCustomXipFolder", "").toString();
    PathSetting.LastExportFolder = settings.value("Path/LastExportFolder", "").toString();
    PathSetting.LastImportFolder = settings.value("Path/LastImportFolder", "").toString();
    PathSetting.LastSelectedItemIndex = settings.value("Path/LastSelectedItemIndex", 0).toInt();

    StateSetting.WindowMaxium = settings.value("State/WindowMaxium", true).toBool();
    StateSetting.MainFrmState = settings.value("State/MainFrmState", QByteArray()).toByteArray();
    StateSetting.ProjectLayoutState = settings.value("State/ProjectLayoutState", QByteArray()).toByteArray();
    StateSetting.MessageLayoutState = settings.value("State/MessageLayoutState", QByteArray()).toByteArray();

    StateSetting.RegEditorMaxium = settings.value("State/RegEditorMaxium", true).toBool();
    StateSetting.RegFrmState = settings.value("State/RegEditorMaxium", QByteArray()).toByteArray();
    StateSetting.KeyLayoutState = settings.value("State/KeyLayoutState", QByteArray()).toByteArray();
}

void SaveAppSettings( void )
{
    QSettings settings(QApplication::applicationDirPath() + "/ConfMemoria.ini",
        QSettings::IniFormat);
    settings.beginGroup("Path");
    settings.setValue("LastBuddyFolder", PathSetting.LastBuddyFolder);
    settings.setValue("LastStockXipFolder", PathSetting.LastStockXipFolder);
    settings.setValue("LastCustomXipFolder", PathSetting.LastCustomXipFolder);
    settings.setValue("LastExportFolder", PathSetting.LastExportFolder);
    settings.setValue("LastImportFolder", PathSetting.LastImportFolder);
    settings.setValue("LastSelectedItemIndex", PathSetting.LastSelectedItemIndex);
    settings.endGroup();

    settings.beginGroup("State");
    settings.setValue("WindowMaxium", StateSetting.WindowMaxium);
    settings.setValue("MainFrmState", StateSetting.MainFrmState);
    settings.setValue("ProjectLayoutState", StateSetting.ProjectLayoutState);
    settings.setValue("MessageLayoutState", StateSetting.MessageLayoutState);

    settings.setValue("RegEditorMaxium", StateSetting.RegEditorMaxium);
    settings.setValue("RegFrmState", StateSetting.RegFrmState);
    settings.setValue("KeyLayoutState", StateSetting.KeyLayoutState);
    settings.endGroup();
}

bool BuildWorkspaceConnection( QString filename, QSqlDatabase& db )
{
    if (db.isValid() == false) {
        db = QSqlDatabase::addDatabase("QSQLITE", "connSQLite");
    }
    db.setDatabaseName(filename);
    return db.open();
}

void BreakWorkspaceConnection(QSqlDatabase& db)
{
    db.close();
    db.setDatabaseName("");
    db.removeDatabase("connSQLite");
    QSqlDatabase::removeDatabase("connSQLite");
}

QString OrdTypeToString( TOrdType type )
{
    switch (type) {
    case otBinary:
        return QObject::tr("Binary Data");
    case otEnum:
        return QObject::tr("Enumerate");
    case otInteger:
        return QObject::tr("Number");
    case otNVarChar:
        return QObject::tr("String");
    }
    return QObject::tr("Error Type");
}

QString OrdTypeToScript( TOrdType type )
{
    switch (type) {
    case otBinary:
        return "VARBINARY";
    case otEnum:
    case otInteger:
        return "INTEGER DEFAULT -1";
    case otNVarChar:
        return "NVARCHAR( 256 )";
    }
    return "";
}

bool AutoAppendSeparator( QString& source, QString separator )
{
    if (source.isEmpty() == false) {
        source += separator;
        return true;
    }
    return false;
}

QString IntToColumnBase( int column ) {
    // 1 = A
    // 26 = Z
    // 27 = AA
    QString Result = "";
    while (column > 0) {
        Result.prepend(QChar('A' + (column - 1) % 26));
        column = (column - 1) / 26;
    }
    if (Result.isEmpty()) {
        Result = "A";
    }
    return Result;
}