#-------------------------------------------------
#
# Project created by QtCreator 2010-05-08T14:53:58
#
#-------------------------------------------------

QT       += core gui webkit xml sql

#TARGET = MemoriaRebuilder
TEMPLATE = app

CONFIG += qt \
    debug_and_relase
CONFIG(debug, debug|release) {
    TARGET = MemoriaRebuilder_d
    DESTDIR = ../Target/GCC/Debug
    MOC_DIR += ./tmp/moc/Debug
    OBJECTS_DIR += ./tmp/obj/Debug
}
else {
    TARGET = MemoriaRebuilder
    DESTDIR = ../Target/GCC/Release
    MOC_DIR += ./tmp/moc/Release
    OBJECTS_DIR += ./tmp/obj/Release
}
DEPENDPATH += .
UI_DIR += ./tmp
RCC_DIR += ./tmp
INCLUDEPATH += ./tmp \
    $MOC_DIR \
    . \
    ./../FvQKOLLite
LIBS += -lole32 \
    -L"./PpsDES"


SOURCES += main.cpp\
        MainUnt.cpp \
    DbCentre.cpp \
    MyDropTreeWidget.cpp

HEADERS  += MainUnt.h \
    DbCentre.h \
    MyDropTreeWidget.h

FORMS    += MainFrm.ui

RESOURCES += \
    MemoriaRebuilder.qrc
