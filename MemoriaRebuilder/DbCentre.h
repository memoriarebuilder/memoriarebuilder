#ifndef _DBCENTRE_H
#define _DBCENTRE_H

#include <QtSql/QSqlDatabase>
#include <QtCore/QString>
#include <QtCore/QVariant>

typedef struct tagPathRecItem {
    QString LastBuddyFolder;
    QString LastStockXipFolder;
    QString LastCustomXipFolder;
    QString LastExportFolder;
    QString LastImportFolder;
    int LastSelectedItemIndex;
} TPathRecItem;

typedef struct tagStateRecItem {
    bool WindowMaxium;
    QByteArray MainFrmState;
    QByteArray ProjectLayoutState;
    QByteArray MessageLayoutState;
    bool RegEditorMaxium;
    QByteArray RegFrmState;
    QByteArray KeyLayoutState;
} TStateRecItem;

extern TPathRecItem PathSetting;
extern TStateRecItem StateSetting;

typedef struct tagSheetRecItem {
    int SheetID;
    QString Title;
    QString Hint;
    QString TableName;
} TSheetRecItem;

enum TOrdType : short {
    otInteger,
    otEnum,
    otNVarChar,
    otBinary
};

typedef struct tagFieldRecItem {
    int FieldID;
    QString Name;
    QString Alias;
    int SheetID;
    TOrdType OrdType;
    bool TitleKey;
    bool IDKey;
} TFieldRecItem;

typedef struct tagPageScrollArgument {
    bool Dirty;
    int TotalCount;
    int CurrentPagesize;
    int Offset;
    bool DistinceData; // no need GroupCache
} TPageScrollArgument;

typedef struct tagGroupCacheRecItem {
    QVariant SubValue;
    int SubCount;
} TGroupCacheRecItem;

void LoadAppSettings( void );
void SaveAppSettings( void );
bool BuildWorkspaceConnection(QString filename, QSqlDatabase& db);
void BreakWorkspaceConnection(QSqlDatabase& db);
QString OrdTypeToString(TOrdType type);
QString OrdTypeToScript(TOrdType type);
bool AutoAppendSeparator(QString& source, QString separator);
QString IntToColumnBase(int column);

#endif
