#include "MainUnt.h"
#include "ui_MainFrm.h"
#include <QFile>
#include "DbCentre.h"
#include <QDebug>
#include <QBuffer>
#include <Windows.h>

TMainFrm::TMainFrm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TMainFrm)
{
    ui->setupUi(this);
    LoadAppSettings();
}

TMainFrm::~TMainFrm()
{
    SaveAppSettings();
    delete ui;
}

void TMainFrm::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

QString decodefielename(QByteArray& data) {
    QByteArray dummy = data;
    for (int i = 0; i < dummy.length(); i++) {
        dummy[i] = (unsigned char)(~((int)dummy[i] ^ dummy.length()));
    }
    return QString::fromLocal8Bit(dummy.data());
}

QString getbrief(int type, QByteArray& data) {
    QString result;
    switch (type) {
        case 1:
            result = data.toHex();
            break;
        case 2:
            // QQNODE_TYPE
            result = data.toHex();
            break;
        case 4:
            {
                if (data.size() == 4) {
                    result = QString("[%1]").arg(*((quint32*)data.data()));
                } else if (data.size() == 2) {
                    result = QString("[%1]").arg(*((quint16*)data.data()));
                } else if (data.size() == 1) {
                    result = QString("[%1]").arg(data[0]);
                } else if (data.size() == 8) {
                    result = QString("[%1]").arg(*((quint64*)data.data()));
                }
            }
            break;
        case 6:
            {
                result = QString("\"%1\"").arg(decodefielename(data));
            }
            break;
        case 10:
            {
                result = data.left(20).toHex();
            }
            break;
        default:
            {
                result = data.left(20).toHex();
            }
            break;
    }
    if (result.length() > 20) {
        result = result.left(20) + "...";
    }
    return result;
}

void TMainFrm::onImportUserClicked()
{
    // use as user.db
    QString userdbfilename = OpenSaveDBDialog.getOpenFileName(this, tr("Open UserDB File"), PathSetting.LastBuddyFolder, "userdb Files (*.db);;All Files (*.*)", 0, 0);
    if (userdbfilename.isEmpty()) {
        return;
    }
    PathSetting.LastBuddyFolder = QFileInfo(userdbfilename).path();

    userdbfilename = userdbfilename.replace('/', '\\');
    IStorage *pStg = NULL;
    LPCOLESTR lpwFileName = (const WCHAR*)userdbfilename.utf16();
    HRESULT hr;

    hr = StgIsStorageFile( lpwFileName );	// Is composite structured storage file?
    if( FAILED(hr) ){
        //printf("Invalid QQ message file: %s\n", fname);
        return;
    }

    hr = StgOpenStorage(
        lpwFileName,        // Filename
        NULL,
        STGM_READ | STGM_SHARE_DENY_WRITE,
        0,
        0,
        &pStg);             // return root storage interface pointer

    if( FAILED(hr) ){
        //printf("Failed to open file %s, errno: 0x%x\n", fname, hr);
        return;
    }

    DWORD mode = STGM_READ | STGM_SHARE_EXCLUSIVE;
    //MsgEx_EnumStorage(pStg);
    IStorage *f1 = NULL, *f2 = NULL;
    pStg->OpenStorage(L"402", NULL, mode, NULL, 0, &f1);
    if (f1 == NULL) {
        return;
    }
    f1->OpenStorage(L"CQQ", NULL, mode, NULL, 0, &f2);
    if (f2 == NULL) {
        return;
    }
    IStream *s1 = NULL;
    f2->OpenStream(L"UserData.dat", NULL, mode, 0, &s1);
    if (s1 == NULL) {
        return;
    }

    STATSTG stat;
    s1->Stat(&stat, STATFLAG_DEFAULT);
    QByteArray userdbcontent(quint64(stat.cbSize.QuadPart), 0);
    ULONG readedbyte;
    s1->Read(userdbcontent.data(), userdbcontent.size(), &readedbyte);
    QBuffer userdbbuf(&userdbcontent);
    userdbbuf.open(QIODevice::ReadOnly);
    BuildNodesViewFromUserData(&userdbbuf);

    if( pStg != NULL ) {
        pStg->Release();
    }
}

void TMainFrm::onImportMsgExClicked()
{
    QString userdatafilename = OpenSaveDBDialog.getOpenFileName(this, tr("Open UserData File"), PathSetting.LastBuddyFolder, "bin Files (*.bin);;All Files (*.*)", 0, 0);
    if (userdatafilename.isEmpty()) {
        return;
    }
    PathSetting.LastBuddyFolder = QFileInfo(userdatafilename).path();

    QFile userdbfile(userdatafilename);
    userdbfile.open(QIODevice::ReadOnly);
    QByteArray userdbcontent = userdbfile.readAll();
    QBuffer userdbbuf(&userdbcontent);
    userdbfile.close();
    userdbbuf.open(QIODevice::ReadOnly);
    BuildNodesViewFromUserData(&userdbbuf);
}

void TMainFrm::BuildNodesViewFromUserData( QIODevice *userdatabuf )
{
    QDataStream reader(userdatabuf);
    reader.setByteOrder(QDataStream::LittleEndian);
    int nodecount = 0;
    int leftfolderfield = -1, parentcontenttail = -1;
    bool waitgroupname = false;
    QList<QTreeWidgetItem *> treeitems;
    ui->nodesView->clear();
    while (reader.atEnd() == false) {
        quint32 MBR;
        quint16 fieldcount;
        bool isnode = false, isfolder = false, arraytail = false;
        if (userdatabuf->pos() == parentcontenttail) {
            // direct to process fields?
            fieldcount = leftfolderfield;
            parentcontenttail = -1;
            arraytail = true;
        } else {
            reader >> MBR;
            if (MBR != 0x01014451) {
#ifdef _DEBUG
                qDebug("userdbfile.pos():0x%X", userdatabuf->pos());
#endif
                //userdbfile.close();
                //return;
                break;
            }
            reader >> fieldcount;
        }
        QStringList itemtext;
        QString num, nickname, count;
#ifdef _DEBUG
        qDebug("record has %d field", fieldcount);
#endif
        for (int i = 0; i < fieldcount; i++) {
            quint8 fieldtype;
            reader >> fieldtype;
            quint16 fieldnamelen;
            reader >> fieldnamelen;
            QByteArray name = userdatabuf->read(fieldnamelen);
            QString latinefieldname = decodefielename(name);
            quint32 contentlen;
            reader >> contentlen;
            QByteArray content;
            if (fieldtype == 10 && latinefieldname == "ChildLink") {
#ifdef _DEBUG
                qDebug("%s:%d 0x%X", latinefieldname.toLatin1().data(), contentlen, userdatabuf->pos());
                qDebug("childcontent: 0x%X - 0x%X", quint32(userdatabuf->pos()), quint32(userdatabuf->pos() + contentlen));
#endif
                if (contentlen < 0xD) {
                    // just bypass contentlen?
                    QByteArray dummy = userdatabuf->read(contentlen); // 51410101 [00000000]
#ifdef _DEBUG
                    qDebug("folder overlay: %s", dummy.toHex().data());
#endif
                    // next
                    arraytail = true;
                    continue;
                } else {
                    isfolder = true;
                    leftfolderfield = fieldcount - i - 1;
                    parentcontenttail = userdatabuf->pos() + contentlen;
                    break; // start a new MBR detection
                }
            } else {
                content = userdatabuf->read(contentlen);
#ifdef _DEBUG
                //qDebug("%s:%d %s", latinefieldname.toLatin1().data(), contentlen, getbrief(fieldtype, content).toLatin1().data());
                //wprintf_s(L"%s:%d %s", latinefieldname.utf16(), contentlen, getbrief(fieldtype, content).utf16());
                //qDebug() << latinefieldname << ":" << contentlen << getbrief(fieldtype, content);
                QString msg = QString("%1 (%2):%3 %4\n").arg(latinefieldname).arg(fieldtype).arg(contentlen).arg(getbrief(fieldtype, content));
                OutputDebugStringW(msg.utf16());
#endif
            }
            switch (fieldtype) {
case 1:
    break;
case 2:
    // QQNODE_TYPE
    break;
case 4:
    {
        // UIN
        if (latinefieldname == "UIN") {
            //num = content;
            if (content.size() == 4) {
                num = QString("%1").arg(*((quint32*)content.data()));
                isnode = true;
            }
        }
    }
    break;
case 6:
    {
        // node -> nickname
        // block -> groupname
        if (latinefieldname == "NAME") {
            nickname = decodefielename(content);
            if (waitgroupname) {
                itemtext << nickname << QString("(%1)").arg(treeitems.size());
                QTreeWidgetItem *treeitem = new QTreeWidgetItem((QTreeWidget*)0, itemtext);
                treeitem->addChildren(treeitems);
                ui->nodesView->addTopLevelItem(treeitem);
                treeitems.clear();
                waitgroupname = false;
            }
        }
    }
    break;
case 10:
    {
        if (latinefieldname == "ChildLink") {

        }
    }
    break;
default:
    break;
            }
        }
        if (isfolder) {
            QByteArray dummy = userdatabuf->read(0xD); // 51 41 01 01 [11 00 00 00] 09 BA 5F 0A 00
#ifdef _DEBUG
            qDebug("folder overlay: %s", dummy.toHex().data());
#endif
        }
        if (isnode) {
            if (userdatabuf->pos() == parentcontenttail) {
#ifdef _DEBUG
                qDebug("last record of node array.");
#endif
                waitgroupname = true;
            } else {
                QByteArray dummy = userdatabuf->read(0x5); // 09 CE 07 00 00
#ifdef _DEBUG
                qDebug("node overlay: %s", dummy.toHex().data());
#endif
                waitgroupname = false;
            }
            nodecount++;
            count = QString("%1").arg(nodecount);
        }
        if (arraytail) {
            QByteArray dummy = userdatabuf->read(0x5); // 09 CE 07 00 00
#ifdef _DEBUG
            qDebug("array overlay: %s", dummy.toHex().data());
#endif
        }
        if (isnode) {
            itemtext << num << nickname << count;
            QTreeWidgetItem *treeitem = new QTreeWidgetItem((QTreeWidget*)0, itemtext);
            treeitems.append(treeitem);
        }
        if (waitgroupname) {
            // name?
        }
    }
    //ui->nodesView->insertTopLevelItems(0, treeitems);
    ui->nodesView->expandAll();
    ui->nodesView->resizeColumnToContents(0);
    ui->nodesView->resizeColumnToContents(1);
    ui->nodesView->resizeColumnToContents(2);
    ui->nodesView->header()->setClickable(true);

    userdatabuf->close();
}
QString localLanguage() {
    QLocale locale = QLocale::system();

    // This switch will translate to the correct locale
    switch (locale.language()) {
        case QLocale::German:
            return "deu";
        case QLocale::French:
            return "fra";
        case QLocale::Chinese:
            return "chs";
        case QLocale::HongKong:
        case QLocale::Macau:
        case QLocale::Taiwan:
            return "cht";
        case QLocale::Spanish:
            return "spa";
        case QLocale::Japanese:
            return "ja";
        case QLocale::Korean:
            return "kor";
        case QLocale::Belgium:
        case QLocale::Netherlands:
        case QLocale::NetherlandsAntilles:
            return "dut";
        default:
            return "enu";
    }
}
